package com.apextrucking.app;

import com.apextrucking.model.snmp.SNMPManager;
import java.io.IOException;

/**
 *
 * @author defalt
 */
public class SNMPManager1 {

    private static SNMPManager client1 = null;
    private static SNMPManager client2 = null;

    public static void main(String[] args) throws IOException {
        client1 = new SNMPManager("udp:127.0.0.1/9876");
        client2 = new SNMPManager("udp:127.0.0.1/9879");
        client1.start();
        client2.start();

        client1.sendToDB(client1.getTruckID(), client1.getTruckDisableIgnition());
        client2.sendToDB(client2.getTruckID(), client2.getTruckDisableIgnition());

        SNMPManager.FuelThread client1_fuel = client1.createFuelThread();
        SNMPManager.FuelThread client2_fuel = client2.createFuelThread();
        SNMPManager.CoordinatesThread client1_coords = client1.createCoordinatesThread();
        SNMPManager.CoordinatesThread client2_coords = client2.createCoordinatesThread();
        SNMPManager.DisableIgnitionThread client1_ignition = client1.createDisableIgnitionThread();
        SNMPManager.DisableIgnitionThread client2_ignition = client2.createDisableIgnitionThread();
        SNMPManager.CheckDisableIgnitionThread client1_checkIgnition = client1.createCheckDisableIgnitionThread();
        SNMPManager.CheckDisableIgnitionThread client2_checkIgnition = client2.createCheckDisableIgnitionThread();
        SNMPManager.DriversThread client1_drivers = client1.createDriversThread();
        SNMPManager.DriversThread client2_drivers = client2.createDriversThread();
        client1_fuel.start();
        client2_fuel.start();
        client1_coords.start();
        client2_coords.start();
        client1_ignition.start();
        client2_ignition.start();
        client1_checkIgnition.start();
        client2_checkIgnition.start();
        client1_drivers.start();
        client2_drivers.start();
    }

}
