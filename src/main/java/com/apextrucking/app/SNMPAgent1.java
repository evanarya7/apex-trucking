package com.apextrucking.app;

import com.apextrucking.model.GPSCoordinates;
import com.apextrucking.model.snmp.SNMPAgent;
import com.apextrucking.model.TruckDriver;
import com.apextrucking.util.Randomizer;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author defalt
 */
public class SNMPAgent1 {

    // INIT VALUES
    private static final String TRUCK_ID = "02A-00135";
    private static final Double TRUCK_SPEED = 60.0;
    private static final Double TRUCK_FUEL = 81.42;
    private static final Double TRUCK_FUEL_CONSUMPTION = 4.0;
    private static final GPSCoordinates TRUCK_COORDS_START = new GPSCoordinates(-6.390932 + Randomizer.getBiasedRandomDouble(-0.000001, 0.000001, 0.0, 0.0000005), 107.399295 + Randomizer.getBiasedRandomDouble(-0.000001, 0.000001, 0.0, 0.0000005));
    private static final GPSCoordinates TRUCK_COORDS_DEST = new GPSCoordinates(-6.358194 + Randomizer.getBiasedRandomDouble(-0.000001, 0.000001, 0.0, 0.0000005), 107.340344 + Randomizer.getBiasedRandomDouble(-0.000001, 0.000001, 0.0, 0.0000005));
    private static final Double TRUCK_TRAVEL_DISTANCE = 7.5;
    private static final Integer TRUCK_DISABLE_IGNITION = 0;
    private static final List<TruckDriver> TRUCK_DRIVERS = Arrays.asList(
            new TruckDriver("Tatang Sutresna", "3203012503770011"),
            new TruckDriver("Asep Subagja", "3274011212720013"));

    private static SNMPAgent agent = null;

    public static void main(String[] args) throws IOException {
        init("udp:127.0.0.1/9876");
        SNMPAgent.SimulationThread t = agent.createSimulationThread(TRUCK_SPEED, TRUCK_FUEL, TRUCK_FUEL_CONSUMPTION, TRUCK_COORDS_START, TRUCK_COORDS_DEST, TRUCK_TRAVEL_DISTANCE);
        t.start();

        while (true) {

        }
    }

    private static void init(String address) throws IOException {
        agent = new SNMPAgent(address);
        agent.start();

        agent.unregisterManagedObject(agent.getSnmpv2MIB());

        agent.getCustomMIB().setTruckID(TRUCK_ID);
        agent.getCustomMIB().setTruckFuel(TRUCK_FUEL.toString());
        agent.getCustomMIB().setTruckLatitude(TRUCK_COORDS_START.getLatitude().toString());
        agent.getCustomMIB().setTruckLongitude(TRUCK_COORDS_START.getLongitude().toString());
        agent.getCustomMIB().setTruckDisableIgnition(TRUCK_DISABLE_IGNITION);
        for (int i = 0; i < TRUCK_DRIVERS.size(); i++) {
            agent.getCustomMIB().addTruckDriverEntryRow(i + 1, TRUCK_DRIVERS.get(i).getName(), TRUCK_DRIVERS.get(i).getIdNumber());
        }
        agent.registerCustomMIB();
    }

}
