package com.apextrucking.model.snmp;

import com.apextrucking.model.GPSCoordinates;
import com.apextrucking.model.TruckDriver;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TreeEvent;
import org.snmp4j.util.TreeUtils;

/**
 *
 * @author defalt
 */
public class SNMPManager {

    private final String MY_OID = "1.3.6.1.4.1.11111.1";
    private final OkHttpClient httpClient = new OkHttpClient();
    private Snmp snmp = null;
    private String address = null;

    /**
     * Constructor
     *
     * @param address
     */
    public SNMPManager(String address) {
        this.address = address;
    }

    /**
     * Start the Snmp session. If you forget the listen() method you will not
     * get any answers because the communication is asynchronous and the
     * listen() method listens for answers.
     *
     * @throws IOException
     */
    public void start() throws IOException {
        TransportMapping transport = new DefaultUdpTransportMapping();
        snmp = new Snmp(transport);
        transport.listen();
    }

    /**
     * Method which takes a single OID and returns the response from the agent
     * as a String.
     *
     * @param oid
     * @return
     * @throws IOException
     */
    private String getAsString(OID oid) throws IOException {
        ResponseEvent event = get(new OID[]{oid});
        return event.getResponse().get(0).getVariable().toString();
    }

    /**
     * This method is capable of handling multiple OIDs
     *
     * @param oids
     * @return
     * @throws IOException
     */
    private ResponseEvent get(OID oids[]) throws IOException {
        PDU pdu = new PDU();
        for (OID oid : oids) {
            pdu.add(new VariableBinding(oid));
        }
        pdu.setType(PDU.GET);

        ResponseEvent event = snmp.send(pdu, getTarget(), null);
        if (event != null) {
            PDU responsePDU = event.getResponse();
            if (responsePDU != null) {
                int errorStatus = responsePDU.getErrorStatus();
                int errorIndex = responsePDU.getErrorIndex();
                String errorStatusText = responsePDU.getErrorStatusText();
                if (errorStatus != PDU.noError) {
                    System.out.println("\nResponse:\nGot Snmp Set Response from Agent");
                    System.out.println("Snmp Set Request = " + event.getRequest().getVariableBindings());
                    System.out.println("\nresponsePDU = " + responsePDU);
                    System.out.println("errorStatus = " + responsePDU);
                    System.out.println("Error: Request Failed");
                    System.out.println("Error Status = " + errorStatus);
                    System.out.println("Error Index = " + errorIndex);
                    System.out.println("Error Status Text = " + errorStatusText);
                }
            }
            return event;
        }
        throw new RuntimeException("GET timed out");
    }

    /**
     * Method which returns the sub tree of the OID.
     *
     * @param rootOid
     * @return
     * @throws IOException
     */
    private List<TreeEvent> getSubTree(OID rootOid) throws IOException {
        PDU pdu = new PDU();
        pdu.add(new VariableBinding(rootOid));
        pdu.setType(PDU.GETBULK);
        TreeUtils treeUtils = new TreeUtils(snmp, new DefaultPDUFactory());
        return treeUtils.getSubtree(getTarget(), rootOid);
    }

    private List<Map<String, String>> getTableAsList(OID rootOid, int columns) throws IOException {
        List<Map<String, String>> list = new ArrayList<>();
        for (int i = 1; i <= columns; i++) {
            List<TreeEvent> tempList = getSubTree(new OID(rootOid.toString() + "." + i));
            for (TreeEvent t : tempList) {
                VariableBinding[] vbs = t.getVariableBindings();
                for (int j = 0; (vbs != null) && j < vbs.length; j++) {
                    if (i == 1) {
                        Map<String, String> map = new HashMap<>();
                        map.put("column" + i, vbs[j].getVariable().toString());
                        list.add(map);
                    } else {
                        Map<String, String> map = list.get(j);
                        map.put("column" + i, vbs[j].getVariable().toString());
                        list.set(j, map);
                    }
                }
            }

        }
        return list;
    }

    /**
     * Method which sets the OID with given value.
     *
     * @param oid
     * @param val
     * @return
     * @throws IOException
     */
    private ResponseEvent set(OID oid, String val) throws IOException {
        PDU pdu = new PDU();
        VariableBinding varBind = new VariableBinding(oid, new OctetString(val));
        pdu.add(varBind);
        pdu.setType(PDU.SET);
        pdu.setRequestID(new Integer32(1));

        ResponseEvent event = snmp.set(pdu, getTarget());
        if (event != null) {
            PDU responsePDU = event.getResponse();
            if (responsePDU != null) {
                int errorStatus = responsePDU.getErrorStatus();
                int errorIndex = responsePDU.getErrorIndex();
                String errorStatusText = responsePDU.getErrorStatusText();
                if (errorStatus != PDU.noError) {
                    System.out.println("\nResponse:\nGot Snmp Set Response from Agent");
                    System.out.println("Snmp Set Request = " + event.getRequest().getVariableBindings());
                    System.out.println("\nresponsePDU = " + responsePDU);
                    System.out.println("errorStatus = " + responsePDU);
                    System.out.println("Error: Request Failed");
                    System.out.println("Error Status = " + errorStatus);
                    System.out.println("Error Index = " + errorIndex);
                    System.out.println("Error Status Text = " + errorStatusText);
                }
            }
            return event;
        }
        throw new RuntimeException("SET timed out");
    }

    /**
     * Method which sets the OID with given value.
     *
     * @param oid
     * @param val
     * @return
     * @throws IOException
     */
    private ResponseEvent set(OID oid, Integer val) throws IOException {
        PDU pdu = new PDU();
        VariableBinding varBind = new VariableBinding(oid, new Integer32(val));
        pdu.add(varBind);
        pdu.setType(PDU.SET);
        pdu.setRequestID(new Integer32(1));

        ResponseEvent event = snmp.set(pdu, getTarget());
        if (event != null) {
            PDU responsePDU = event.getResponse();
            if (responsePDU != null) {
                int errorStatus = responsePDU.getErrorStatus();
                int errorIndex = responsePDU.getErrorIndex();
                String errorStatusText = responsePDU.getErrorStatusText();
                if (errorStatus != PDU.noError) {
                    System.out.println("\nResponse:\nGot Snmp Set Response from Agent");
                    System.out.println("Snmp Set Request = " + event.getRequest().getVariableBindings());
                    System.out.println("\nresponsePDU = " + responsePDU);
                    System.out.println("errorStatus = " + responsePDU);
                    System.out.println("Error: Request Failed");
                    System.out.println("Error Status = " + errorStatus);
                    System.out.println("Error Index = " + errorIndex);
                    System.out.println("Error Status Text = " + errorStatusText);
                }
            }
            return event;
        }
        throw new RuntimeException("SET timed out");
    }

    /**
     * This method returns a Target, which contains information about where the
     * data should be fetched and how.
     *
     * @return
     */
    private Target getTarget() {
        Address targetAddress = GenericAddress.parse(address);
        CommunityTarget target = new CommunityTarget();
        target.setCommunity(new OctetString("public"));
        target.setAddress(targetAddress);
        target.setRetries(2);
        target.setTimeout(1500);
        target.setVersion(SnmpConstants.version2c);
        return target;
    }

    public String getTruckID() throws IOException {
        return getAsString(new OID(MY_OID + ".1.0"));
    }

    public Double getTruckFuel() throws IOException {
        return Double.valueOf(getAsString(new OID(MY_OID + ".2.0")));
    }

    public GPSCoordinates getTruckCoordinates() throws IOException {
        return new GPSCoordinates(
                Double.valueOf(getAsString(new OID(MY_OID + ".3.1.0"))), Double.valueOf(getAsString(new OID(MY_OID + ".3.2.0"))));
    }

    public Integer getTruckDisableIgnition() throws IOException {
        return Integer.valueOf(getAsString(new OID(MY_OID + ".4.0")));
    }

    public Integer getTruckDisableIgnitionFromDB() throws IOException {
        String db = "apextrucking";
        String q = "SELECT \"value\" FROM \"disableIgnition\" WHERE \"truck\"='" + getTruckID() + "'";
        Request request = new Request.Builder()
                .url("http://localhost:8086/query?db=" + db + "&q=" + URLEncoder.encode(q, StandardCharsets.UTF_8))
                .get()
                .build();

        try (Response response = httpClient.newCall(request).execute()) {
            if (response.isSuccessful()) {
                JSONArray arr = new JSONObject(new JSONObject(new JSONObject(response.body().string()).getJSONArray("results").get(0).toString()).getJSONArray("series").get(0).toString()).getJSONArray("values");
                return Integer.valueOf(new JSONArray(arr.get(arr.length() - 1).toString()).get(1).toString());
            } else {
                throw new IOException("Unexpected code " + response);
            }
        }
    }

    public void setTruckDisableIgnition(Integer value) throws IOException {
        set(new OID(MY_OID + ".4.0"), value);
    }

    public List<TruckDriver> getTruckDrivers() throws IOException {
        List<TruckDriver> list = new ArrayList<>();
        List<Map<String, String>> drivers = getTableAsList(new OID(MY_OID + ".5.1"), 3);
        drivers.forEach((m) -> {
            list.add(new TruckDriver(m.get("column2"), m.get("column3")));
        });
        return list;
    }

    public void sendToDB(String truckID, Double truckFuel) throws IOException {
        RequestBody formBody = RequestBody.create(
                "fuel,"
                + "truck=" + truckID
                + " value=" + truckFuel, MediaType.parse("text/plain"));
        Request request = new Request.Builder()
                .url("http://localhost:8086/write?db=apextrucking")
                .post(formBody)
                .build();

        try (Response response = httpClient.newCall(request).execute()) {
            if (response.isSuccessful()) {
                System.out.println("Fuel data saved to DB.");
            } else {
                throw new IOException("Unexpected code " + response);
            }
        }
    }

    public void sendToDB(String truckID, GPSCoordinates truckCoordinates) throws IOException {
        RequestBody formBody = RequestBody.create(
                "gps,"
                + "truck=" + truckID
                + " latitude=" + truckCoordinates.getLatitude()
                + ",longitude=" + truckCoordinates.getLongitude(), MediaType.parse("text/plain"));
        Request request = new Request.Builder()
                .url("http://localhost:8086/write?db=apextrucking")
                .post(formBody)
                .build();

        try (Response response = httpClient.newCall(request).execute()) {
            if (response.isSuccessful()) {
                System.out.println("GPS data saved to DB.");
            } else {
                throw new IOException("Unexpected code " + response);
            }
        }
    }

    public void sendToDB(String truckID, Integer truckDisableIgnition) throws IOException {
        RequestBody formBody = RequestBody.create(
                "disableIgnition,"
                + "truck=" + truckID
                + " value=" + truckDisableIgnition, MediaType.parse("text/plain"));
        Request request = new Request.Builder()
                .url("http://localhost:8086/write?db=apextrucking")
                .post(formBody)
                .build();

        try (Response response = httpClient.newCall(request).execute()) {
            if (response.isSuccessful()) {
                System.out.println("Disable ignition data saved to DB.");
            } else {
                throw new IOException("Unexpected code " + response);
            }
        }
    }

    public void sendToDB(String truckID, List<TruckDriver> truckDrivers) throws IOException {
        truckDrivers.forEach((d) -> {
            RequestBody formBody = RequestBody.create(
                    "drivers,"
                    + "truck=" + truckID
                    + " name=" + "\"" + d.getName() + "\""
                    + ",idNumber=" + d.getIdNumber(), MediaType.parse("text/plain"));
            Request request = new Request.Builder()
                    .url("http://localhost:8086/write?db=apextrucking")
                    .post(formBody)
                    .build();

            try (Response response = httpClient.newCall(request).execute()) {
                if (response.isSuccessful()) {
                    System.out.println("Driver data saved to DB.");
                } else {
                    throw new IOException("Unexpected code " + response);
                }
            } catch (IOException ex) {
                System.out.println("Error saving data to DB.");
                System.out.println(ex);
            }
        });
    }

    public FuelThread createFuelThread() {
        return new FuelThread();
    }

    public CoordinatesThread createCoordinatesThread() {
        return new CoordinatesThread();
    }

    public DisableIgnitionThread createDisableIgnitionThread() {
        return new DisableIgnitionThread();
    }
    
    public CheckDisableIgnitionThread createCheckDisableIgnitionThread() {
        return new CheckDisableIgnitionThread();
    }

    public DriversThread createDriversThread() {
        return new DriversThread();
    }

    public class FuelThread extends Thread {

        private final Integer REFRESH_INTERVAL = 10 * 1000;

        @Override
        public void run() {
            while (true) {
                try {
                    sendToDB(getTruckID(), getTruckFuel());

                    Thread.sleep(REFRESH_INTERVAL);
                } catch (InterruptedException | IOException ex) {
                    System.out.println(ex);
                }
            }

        }
    }

    public class CoordinatesThread extends Thread {

        private final Integer REFRESH_INTERVAL = 5 * 1000;

        @Override
        public void run() {
            while (true) {
                try {
                    sendToDB(getTruckID(), getTruckCoordinates());

                    Thread.sleep(REFRESH_INTERVAL);
                } catch (InterruptedException | IOException ex) {
                    System.out.println(ex);
                }
            }

        }
    }
    
    public class DisableIgnitionThread extends Thread {

        private final Integer REFRESH_INTERVAL = 5 * 60 * 1000;

        @Override
        public void run() {
            while (true) {
                try {
                    Integer fromDB = getTruckDisableIgnitionFromDB();
                    Integer fromAgent = getTruckDisableIgnition();

                    if (Objects.equals(fromDB, fromAgent)) {
                        sendToDB(getTruckID(), fromAgent);
                    }

                    Thread.sleep(REFRESH_INTERVAL);
                } catch (InterruptedException | IOException ex) {
                    System.out.println(ex);
                }
            }

        }
    }

    public class CheckDisableIgnitionThread extends Thread {

        private final Integer REFRESH_INTERVAL = 3 * 1000;

        @Override
        public void run() {
            while (true) {
                try {
                    Integer fromDB = getTruckDisableIgnitionFromDB();
                    Integer fromAgent = getTruckDisableIgnition();

                    if (!Objects.equals(fromDB, fromAgent)) {
                        setTruckDisableIgnition(fromDB);
                    }

                    Thread.sleep(REFRESH_INTERVAL);
                } catch (InterruptedException | IOException ex) {
                    System.out.println(ex);
                }
            }

        }
    }

    public class DriversThread extends Thread {

        private final Integer REFRESH_INTERVAL = 5 * 60 * 1000;

        @Override
        public void run() {
            while (true) {
                try {
                    sendToDB(getTruckID(), getTruckDrivers());

                    Thread.sleep(REFRESH_INTERVAL);
                } catch (InterruptedException | IOException ex) {
                    System.out.println(ex);
                }
            }

        }
    }

}
