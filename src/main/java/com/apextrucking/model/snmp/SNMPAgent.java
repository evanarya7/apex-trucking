package com.apextrucking.model.snmp;

import com.apextrucking.model.GPSCoordinates;
import com.apextrucking.util.Randomizer;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.snmp4j.TransportMapping;
import org.snmp4j.agent.BaseAgent;
import org.snmp4j.agent.CommandProcessor;
import org.snmp4j.agent.DuplicateRegistrationException;
import org.snmp4j.agent.MOGroup;
import org.snmp4j.agent.MOServer;
import org.snmp4j.agent.ManagedObject;
import org.snmp4j.agent.mo.DefaultMOFactory;
import org.snmp4j.agent.mo.MOFactory;
import org.snmp4j.agent.mo.MOTableRow;
import org.snmp4j.agent.mo.snmp.RowStatus;
import org.snmp4j.agent.mo.snmp.SnmpCommunityMIB;
import org.snmp4j.agent.mo.snmp.SnmpNotificationMIB;
import org.snmp4j.agent.mo.snmp.SnmpTargetMIB;
import org.snmp4j.agent.mo.snmp.StorageType;
import org.snmp4j.agent.mo.snmp.VacmMIB;
import org.snmp4j.agent.security.MutableVACM;
import org.snmp4j.mp.MPv3;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModel;
import org.snmp4j.security.USM;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.Variable;
import org.snmp4j.transport.TransportMappings;

/**
 *
 * @author defalt
 */
public class SNMPAgent extends BaseAgent {

    private final String address;
    private final CustomMIB customMIB;

    /**
     *
     * @param address
     * @throws IOException
     */
    public SNMPAgent(String address) throws IOException {

        /**
         * Creates a base agent with boot-counter, config file, and a
         * CommandProcessor for processing SNMP requests. Parameters:
         * "bootCounterFile" - a file with serialized boot-counter information
         * (read/write). If the file does not exist it is created on shutdown of
         * the agent. "configFile" - a file with serialized configuration
         * information (read/write). If the file does not exist it is created on
         * shutdown of the agent. "commandProcessor" - the CommandProcessor
         * instance that handles the SNMP requests.
         */
        super(new File("conf.agent"), new File("bootCounter.agent"),
                new CommandProcessor(
                        new OctetString(MPv3.createLocalEngineID())));
        customMIB = new CustomMIB(DefaultMOFactory.getInstance());
        this.address = address;
    }

    /**
     * Adds community to security name mappings needed for SNMPv1 and SNMPv2c.
     *
     * @param communityMIB
     */
    @Override
    protected void addCommunities(SnmpCommunityMIB communityMIB) {
        Variable[] com2sec = new Variable[]{
            new OctetString("public"),
            new OctetString("cpublic"), // security name
            getAgent().getContextEngineID(), // local engine ID
            new OctetString("public"), // default context name
            new OctetString(), // transport tag
            new Integer32(StorageType.nonVolatile), // storage type
            new Integer32(RowStatus.active) // row status
        };
        MOTableRow row = communityMIB.getSnmpCommunityEntry().createRow(
                new OctetString("public2public").toSubIndex(true), com2sec);
        communityMIB.getSnmpCommunityEntry().addRow((SnmpCommunityMIB.SnmpCommunityEntryRow) row);
    }

    /**
     * Adds initial notification targets and filters.
     *
     * @param arg0
     * @param arg1
     */
    @Override
    protected void addNotificationTargets(SnmpTargetMIB arg0,
            SnmpNotificationMIB arg1) {
        // TODO Auto-generated method stub

    }

    /**
     * Adds all the necessary initial users to the USM.
     *
     * @param arg0
     */
    @Override
    protected void addUsmUser(USM arg0) {
        // TODO Auto-generated method stub

    }

    /**
     * Adds initial VACM configuration.
     *
     * @param vacm
     */
    @Override
    protected void addViews(VacmMIB vacm) {
        vacm.addGroup(SecurityModel.SECURITY_MODEL_SNMPv2c, new OctetString(
                "cpublic"), new OctetString("v1v2group"),
                StorageType.nonVolatile);

        vacm.addAccess(new OctetString("v1v2group"), new OctetString("public"),
                SecurityModel.SECURITY_MODEL_ANY, SecurityLevel.NOAUTH_NOPRIV,
                MutableVACM.VACM_MATCH_EXACT, new OctetString("fullReadView"),
                new OctetString("fullWriteView"), new OctetString(
                        "fullNotifyView"), StorageType.nonVolatile);

        vacm.addViewTreeFamily(new OctetString("fullReadView"), new OID("1.3"),
                new OctetString(), VacmMIB.vacmViewIncluded,
                StorageType.nonVolatile);

        vacm.addViewTreeFamily(new OctetString("fullWriteView"), new OID("1.3"),
                new OctetString(), VacmMIB.vacmViewIncluded,
                StorageType.nonVolatile);
    }

    /**
     * Unregister the basic MIB modules from the agent's MOServer.
     */
    @Override
    protected void unregisterManagedObjects() {
        // TODO Auto-generated method stub

    }

    /**
     * Register additional managed objects at the agent's server.
     */
    @Override
    protected void registerManagedObjects() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void initTransportMappings() throws IOException {
        transportMappings = new TransportMapping[1];
        Address addr = GenericAddress.parse(address);
        TransportMapping tm = TransportMappings.getInstance()
                .createTransportMapping(addr);
        transportMappings[0] = tm;
    }

    /**
     * Start method invokes some initialization methods needed to start the
     * agent
     *
     * @return
     * @throws IOException
     */
    public void start() throws IOException {

        init();
        // This method reads some old config from a file and causes
        // unexpected behavior.
        // loadConfig(ImportModes.REPLACE_CREATE);
        addShutdownHook();
        getServer().addContext(new OctetString("public"));
        finishInit();
        run();
        sendColdStartNotification();
    }

    /**
     * Register the custom MIB
     *
     */
    public void registerCustomMIB() {
        try {
            customMIB.registerMOs(server, null);
        } catch (DuplicateRegistrationException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Register the custom MIB
     *
     */
    public void unregisterCustomMIB() {
        customMIB.unregisterMOs(server, null);
    }

    public void unregisterManagedObject(MOGroup moGroup) {
        moGroup.unregisterMOs(server, getContext(moGroup));
    }

    public CustomMIB getCustomMIB() {
        return customMIB;
    }

    private Double simulateFuel(Double speed, Double fuel, Double fuelConsumption, Integer timeElapsed) {
        speed = speed / 3600000;
        Double newFuel = fuel - (speed / fuelConsumption * timeElapsed) + Randomizer.getBiasedRandomDouble(-0.01, 0.01, 0.0, 0.005);

        return newFuel;
    }

    private GPSCoordinates calculateGPSMovement(GPSCoordinates start, GPSCoordinates destination, Integer estimatedTravelTime, Integer refreshInterval) {
        return new GPSCoordinates(Math.abs(start.getLatitude() - destination.getLatitude()) / (estimatedTravelTime / refreshInterval), Math.abs(start.getLongitude() - destination.getLongitude()) / (estimatedTravelTime / refreshInterval));
    }

    private GPSCoordinates simulateGPS(GPSCoordinates start, GPSCoordinates destination, GPSCoordinates current, GPSCoordinates gpsMovement, Integer estimatedTravelTime, Integer timeElapsed) {
        if (timeElapsed < estimatedTravelTime) {
            Double newLatitude;
            Double newLongitude;
            if (start.getLatitude() < destination.getLatitude()) {
                newLatitude = current.getLatitude() + gpsMovement.getLatitude() + Randomizer.getBiasedRandomDouble(-0.000001, 0.000001, 0.0, 0.0000005);
            } else {
                newLatitude = current.getLatitude() - gpsMovement.getLatitude() + Randomizer.getBiasedRandomDouble(-0.000001, 0.000001, 0.0, 0.0000005);
            }
            if (start.getLongitude() < destination.getLongitude()) {
                newLongitude = current.getLongitude() + gpsMovement.getLongitude() + Randomizer.getBiasedRandomDouble(-0.000001, 0.000001, 0.0, 0.0000005);
            } else {
                newLongitude = current.getLongitude() - gpsMovement.getLongitude() + Randomizer.getBiasedRandomDouble(-0.000001, 0.000001, 0.0, 0.0000005);
            }

            return new GPSCoordinates(newLatitude, newLongitude);
        } else {
            return current;
        }
    }

    public SimulationThread createSimulationThread(Double truckSpeed, Double truckFuel, Double truckFuelConsumption, GPSCoordinates truckCoordsStart, GPSCoordinates TruckCoordsDestination, Double truckTravelDistance) {
        return new SimulationThread(truckSpeed, truckFuel, truckFuelConsumption, truckCoordsStart, TruckCoordsDestination, truckTravelDistance);
    }

    public class SimulationThread extends Thread {

        private final Integer REFRESH_INTERVAL = 5000;
        private final Double TRUCK_SPEED;
        private final Double TRUCK_FUEL;
        private final Double TRUCK_FUEL_CONSUMPTION;
        private final GPSCoordinates TRUCK_COORDS_START;
        private final GPSCoordinates TRUCK_COORDS_DEST;
        private final Integer EST_TRAVEL_TIME;
        private final GPSCoordinates GPS_MOVEMENT;

        private Integer timeElapsed = 0;
        private Double truckFuelCurrent;
        private GPSCoordinates truckCoordsCurrent;

        public SimulationThread(Double truckSpeed, Double truckFuel, Double truckFuelConsumption, GPSCoordinates truckCoordsStart, GPSCoordinates TruckCoordsDestination, Double truckTravelDistance) {
            this.truckFuelCurrent = truckFuel;
            this.truckCoordsCurrent = truckCoordsStart;
            TRUCK_SPEED = truckSpeed;
            TRUCK_FUEL = truckFuel;
            TRUCK_FUEL_CONSUMPTION = truckFuelConsumption;
            TRUCK_COORDS_START = truckCoordsStart;
            TRUCK_COORDS_DEST = TruckCoordsDestination;

            EST_TRAVEL_TIME = (int) Math.round(truckTravelDistance / truckSpeed * 3600 * 1000);
            GPS_MOVEMENT = calculateGPSMovement(truckCoordsStart, TruckCoordsDestination, EST_TRAVEL_TIME, REFRESH_INTERVAL);
        }

        @Override
        public void run() {
            while (true) {
                try {
                    if (timeElapsed < EST_TRAVEL_TIME) {
                        truckFuelCurrent = simulateFuel(TRUCK_SPEED, TRUCK_FUEL, TRUCK_FUEL_CONSUMPTION, timeElapsed);
                        truckCoordsCurrent = simulateGPS(TRUCK_COORDS_START, TRUCK_COORDS_DEST, truckCoordsCurrent, GPS_MOVEMENT, EST_TRAVEL_TIME, timeElapsed);

                        unregisterCustomMIB();
                        customMIB.setTruckFuel(truckFuelCurrent.toString());
                        customMIB.setTruckLatitude(truckCoordsCurrent.getLatitude().toString());
                        customMIB.setTruckLongitude(truckCoordsCurrent.getLongitude().toString());
                        registerCustomMIB();

                        Thread.sleep(REFRESH_INTERVAL);
                        timeElapsed += REFRESH_INTERVAL;
                    } else {
                        break;
                    }
                } catch (InterruptedException e) {
                }
            }
        }

    }
}
