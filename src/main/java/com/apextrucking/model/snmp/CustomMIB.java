package com.apextrucking.model.snmp;

//--AgentGen BEGIN=_BEGIN
//--AgentGen END
import org.snmp4j.smi.*;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.agent.*;
import org.snmp4j.agent.mo.*;
import org.snmp4j.agent.mo.snmp.*;
import org.snmp4j.agent.request.*;
import org.snmp4j.log.LogFactory;
import org.snmp4j.log.LogAdapter;

//--AgentGen BEGIN=_IMPORT
//--AgentGen END
public class CustomMIB
        //--AgentGen BEGIN=_EXTENDS
        //--AgentGen END
        implements MOGroup //--AgentGen BEGIN=_IMPLEMENTS
//--AgentGen END
{

    private static final LogAdapter LOGGER
            = LogFactory.getLogger(CustomMIB.class);

//--AgentGen BEGIN=_STATIC
//--AgentGen END
    // Factory
    private MOFactory moFactory
            = DefaultMOFactory.getInstance();

    // Constants 
    /**
     * OID of this MIB module for usage which can be used for its
     * identification.
     */
    public static final OID oidApextruckingSnmpMib
            = new OID(new int[]{1, 3, 6, 1, 4, 1, 11111});

    // Identities
    public static final OID oidApexTruckingObjects
            = new OID(new int[]{1, 3, 6, 1, 4, 1, 11111, 1});
    public static final OID oidTruckCoordinates
            = new OID(new int[]{1, 3, 6, 1, 4, 1, 11111, 1, 3});
    public static final OID oidApexTruckingConf
            = new OID(new int[]{1, 3, 6, 1, 4, 1, 11111, 2});
    public static final OID oidApexTruckingGroups
            = new OID(new int[]{1, 3, 6, 1, 4, 1, 11111, 2, 1});
    public static final OID oidApexTruckingCompliances
            = new OID(new int[]{1, 3, 6, 1, 4, 1, 11111, 2, 2});
    // Scalars
    public static final OID oidTruckID
            = new OID(new int[]{1, 3, 6, 1, 4, 1, 11111, 1, 1, 0});
    public static final OID oidTruckFuel
            = new OID(new int[]{1, 3, 6, 1, 4, 1, 11111, 1, 2, 0});
    public static final OID oidTruckLatitude
            = new OID(new int[]{1, 3, 6, 1, 4, 1, 11111, 1, 3, 1, 0});
    public static final OID oidTruckLongitude
            = new OID(new int[]{1, 3, 6, 1, 4, 1, 11111, 1, 3, 2, 0});
    public static final OID oidTruckDisableIgnition
            = new OID(new int[]{1, 3, 6, 1, 4, 1, 11111, 1, 4, 0});
    // Tables

    // Notifications
    // Enumerations
    public static final class TruckDisableIgnitionEnum {

        public static final int allow = 0;
        public static final int disable = 1;
    }

    // TextualConventions
    // Scalars
    private MOScalar<OctetString> truckID;
    private MOScalar<OctetString> truckFuel;
    private MOScalar<OctetString> truckLatitude;
    private MOScalar<OctetString> truckLongitude;
    private EnumeratedScalar truckDisableIgnition;

    // Tables
    public static final OID oidTruckDriversEntry
            = new OID(new int[]{1, 3, 6, 1, 4, 1, 11111, 1, 5, 1});

    // Index OID definitions
    public static final OID oidTruckDriversEntryIndex
            = new OID(new int[]{1, 3, 6, 1, 4, 1, 11111, 1, 5, 1, 1});

    // Column TC definitions for truckDriversEntry:
    // Column sub-identifier definitions for truckDriversEntry:
    public static final int colTruckDriversEntryIndex = 1;
    public static final int colTruckDriversEntryName = 2;
    public static final int colTruckDriversEntryIDNumber = 3;

    // Column index definitions for truckDriversEntry:
    public static final int idxTruckDriversEntryIndex = 0;
    public static final int idxTruckDriversEntryName = 1;
    public static final int idxTruckDriversEntryIDNumber = 2;

    private MOTableSubIndex[] truckDriversEntryIndexes;
    private MOTableIndex truckDriversEntryIndex;

    private MOTable<TruckDriversEntryRow, MOColumn, MOTableModel<TruckDriversEntryRow>> truckDriversEntry;
    private MOTableModel<TruckDriversEntryRow> truckDriversEntryModel;

//--AgentGen BEGIN=_MEMBERS
//--AgentGen END
    /**
     * Constructs a ApextruckingSnmpMib instance without actually creating its
     * <code>ManagedObject</code> instances. This has to be done in a sub-class
     * constructor or after construction by calling
     * {@link #createMO(MOFactory moFactory)}.
     */
    protected CustomMIB() {
//--AgentGen BEGIN=_DEFAULTCONSTRUCTOR
//--AgentGen END
    }

    /**
     * Constructs a ApextruckingSnmpMib instance and actually creates its
     * <code>ManagedObject</code> instances using the supplied
     * <code>MOFactory</code> (by calling
     * {@link #createMO(MOFactory moFactory)}).
     *
     * @param moFactory the <code>MOFactory</code> to be used to create the
     * managed objects for this module.
     */
    public CustomMIB(MOFactory moFactory) {
        this();
        //--AgentGen BEGIN=_FACTORYCONSTRUCTOR::factoryWrapper
        //--AgentGen END
        this.moFactory = moFactory;
        createMO(moFactory);
//--AgentGen BEGIN=_FACTORYCONSTRUCTOR
//--AgentGen END
    }

//--AgentGen BEGIN=_CONSTRUCTORS
//--AgentGen END
    /**
     * Create the ManagedObjects defined for this MIB module using the specified
     * {@link MOFactory}.
     *
     * @param moFactory the <code>MOFactory</code> instance to use for object
     * creation.
     */
    protected void createMO(MOFactory moFactory) {
        addTCsToFactory(moFactory);
        truckID
                = moFactory.createScalar(oidTruckID,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY),
                        new OctetString());
        truckFuel
                = moFactory.createScalar(oidTruckFuel,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY),
                        new OctetString());
        truckLatitude
                = moFactory.createScalar(oidTruckLatitude,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY),
                        new OctetString());
        truckLongitude
                = moFactory.createScalar(oidTruckLongitude,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY),
                        new OctetString());
        truckDisableIgnition
                = new TruckDisableIgnition(oidTruckDisableIgnition,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_WRITE));
        truckDisableIgnition.addMOValueValidationListener(new TruckDisableIgnitionValidator());
        createTruckDriversEntry(moFactory);
    }

    public MOScalar<OctetString> getTruckID() {
        return truckID;
    }

    public MOScalar<OctetString> getTruckFuel() {
        return truckFuel;
    }

    public MOScalar<OctetString> getTruckLatitude() {
        return truckLatitude;
    }

    public MOScalar<OctetString> getTruckLongitude() {
        return truckLongitude;
    }

    public EnumeratedScalar getTruckDisableIgnition() {
        return truckDisableIgnition;
    }

    public void setTruckID(String truckID) {
        this.truckID
                = moFactory.createScalar(oidTruckID,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY),
                        new OctetString(truckID));
    }

    public void setTruckFuel(String truckFuel) {
        this.truckFuel
                = moFactory.createScalar(oidTruckFuel,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY),
                        new OctetString(truckFuel));
    }

    public void setTruckLatitude(String truckLatitude) {
        this.truckLatitude = moFactory.createScalar(oidTruckLatitude,
                moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY),
                new OctetString(truckLatitude));
    }

    public void setTruckLongitude(String truckLongitude) {
        this.truckLongitude = moFactory.createScalar(oidTruckLongitude,
                moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY),
                new OctetString(truckLongitude));
    }

    public void setTruckDisableIgnition(Integer truckDisableIgnition) {
        this.truckDisableIgnition = new TruckDisableIgnition(oidTruckDisableIgnition,
                moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_WRITE), truckDisableIgnition);
        this.truckDisableIgnition.addMOValueValidationListener(new TruckDisableIgnitionValidator());
    }

    public MOTable<TruckDriversEntryRow, MOColumn, MOTableModel<TruckDriversEntryRow>> getTruckDriversEntry() {
        return truckDriversEntry;
    }

    public void addTruckDriverEntryRow(Integer index, String truckDriverName, String truckDriverIDNumber) {
        truckDriversEntry.addRow(truckDriversEntry.createRow(new OID(index.toString()), new Variable[]{new Integer32(index), new OctetString(truckDriverName), new OctetString(truckDriverIDNumber)}));
    }

    @SuppressWarnings(value = {"unchecked"})
    private void createTruckDriversEntry(MOFactory moFactory) {
        // Index definition
        truckDriversEntryIndexes
                = new MOTableSubIndex[]{
                    moFactory.createSubIndex(oidTruckDriversEntryIndex,
                            SMIConstants.SYNTAX_INTEGER, 1, 1)
                };

        truckDriversEntryIndex
                = moFactory.createIndex(truckDriversEntryIndexes,
                        false,
                        new MOTableIndexValidator() {
                    @Override
                    public boolean isValidIndex(OID index) {
                        boolean isValidIndex = true;
                        //--AgentGen BEGIN=truckDriversEntry::isValidIndex
                        //--AgentGen END
                        return isValidIndex;
                    }
                });

        // Columns
        MOColumn[] truckDriversEntryColumns = new MOColumn[3];
        truckDriversEntryColumns[idxTruckDriversEntryIndex]
                = moFactory.createColumn(colTruckDriversEntryIndex,
                        SMIConstants.SYNTAX_INTEGER,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY));
        truckDriversEntryColumns[idxTruckDriversEntryName]
                = moFactory.createColumn(colTruckDriversEntryName,
                        SMIConstants.SYNTAX_OCTET_STRING,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY));
        truckDriversEntryColumns[idxTruckDriversEntryIDNumber]
                = moFactory.createColumn(colTruckDriversEntryIDNumber,
                        SMIConstants.SYNTAX_OCTET_STRING,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY));
        // Table model
        truckDriversEntryModel
                = moFactory.createTableModel(oidTruckDriversEntry,
                        truckDriversEntryIndex,
                        truckDriversEntryColumns);
        ((MOMutableTableModel<TruckDriversEntryRow>) truckDriversEntryModel).setRowFactory(
                new TruckDriversEntryRowFactory());
        truckDriversEntry
                = moFactory.createTable(oidTruckDriversEntry,
                        truckDriversEntryIndex,
                        truckDriversEntryColumns,
                        truckDriversEntryModel);
    }

    @Override
    public void registerMOs(MOServer server, OctetString context)
            throws DuplicateRegistrationException {
        // Scalar Objects
        server.register(this.truckID, context);
        server.register(this.truckFuel, context);
        server.register(this.truckLatitude, context);
        server.register(this.truckLongitude, context);
        server.register(this.truckDisableIgnition, context);
        server.register(this.truckDriversEntry, context);
//--AgentGen BEGIN=_registerMOs
//--AgentGen END
    }

    @Override
    public void unregisterMOs(MOServer server, OctetString context) {
        // Scalar Objects
        server.unregister(this.truckID, context);
        server.unregister(this.truckFuel, context);
        server.unregister(this.truckLatitude, context);
        server.unregister(this.truckLongitude, context);
        server.unregister(this.truckDisableIgnition, context);
        server.unregister(this.truckDriversEntry, context);
//--AgentGen BEGIN=_unregisterMOs
//--AgentGen END
    }

    // Notifications
    // Scalars
    public class TruckDisableIgnition extends EnumeratedScalar<Integer32> {

        TruckDisableIgnition(OID oid, MOAccess access) {
            super(oid, access, new Integer32(),
                    new int[]{TruckDisableIgnitionEnum.allow,
                        TruckDisableIgnitionEnum.disable});
            setValue(new Integer32(0));
//--AgentGen BEGIN=truckDisableIgnition
//--AgentGen END
        }

        TruckDisableIgnition(OID oid, MOAccess access, Integer value) {
            super(oid, access, new Integer32());
            setValue(new Integer32(value));
        }

        @Override
        public int isValueOK(SubRequest request) {
            Variable newValue
                    = request.getVariableBinding().getVariable();
            int valueOK = super.isValueOK(request);
            if (valueOK != SnmpConstants.SNMP_ERROR_SUCCESS) {
                return valueOK;
            }
            //--AgentGen BEGIN=truckDisableIgnition::isValueOK
            //--AgentGen END
            return valueOK;
        }

        @Override
        public Integer32 getValue() {
            //--AgentGen BEGIN=truckDisableIgnition::getValue
            //--AgentGen END
            return super.getValue();
        }

        @Override
        public int setValue(Integer32 newValue) {
            //--AgentGen BEGIN=truckDisableIgnition::setValue
            //--AgentGen END
            return super.setValue(newValue);
        }

        //--AgentGen BEGIN=truckDisableIgnition::_METHODS
        //--AgentGen END
    }

    // Value Validators
    /**
     * The <code>TruckDisableIgnitionValidator</code> implements the value
     * validation for <code>TruckDisableIgnition</code>.
     */
    static class TruckDisableIgnitionValidator implements MOValueValidationListener {

        @Override
        public void validate(MOValueValidationEvent validationEvent) {
            Variable newValue = validationEvent.getNewValue();
            //--AgentGen BEGIN=truckDisableIgnition::validate
            //--AgentGen END
        }
    }

    // Rows and Factories
    public class TruckDriversEntryRow extends DefaultMOMutableRow2PC {

        //--AgentGen BEGIN=truckDriversEntry::RowMembers
        //--AgentGen END
        public TruckDriversEntryRow(OID index, Variable[] values) {
            super(index, values);
            //--AgentGen BEGIN=truckDriversEntry::RowConstructor
            //--AgentGen END
        }

        public Integer32 getTruckDriversEntryIndex() {
            //--AgentGen BEGIN=truckDriversEntry::getTruckDriversEntryIndex
            //--AgentGen END
            return (Integer32) super.getValue(idxTruckDriversEntryIndex);
        }

        public void setTruckDriversEntryIndex(Integer32 newColValue) {
            //--AgentGen BEGIN=truckDriversEntry::setTruckDriversEntryIndex
            //--AgentGen END
            super.setValue(idxTruckDriversEntryIndex, newColValue);
        }

        public OctetString getTruckDriversEntryName() {
            //--AgentGen BEGIN=truckDriversEntry::getTruckDriversEntryName
            //--AgentGen END
            return (OctetString) super.getValue(idxTruckDriversEntryName);
        }

        public void setTruckDriversEntryName(OctetString newColValue) {
            //--AgentGen BEGIN=truckDriversEntry::setTruckDriversEntryName
            //--AgentGen END
            super.setValue(idxTruckDriversEntryName, newColValue);
        }

        public OctetString getTruckDriversEntryIDNumber() {
            //--AgentGen BEGIN=truckDriversEntry::getTruckDriversEntryIDNumber
            //--AgentGen END
            return (OctetString) super.getValue(idxTruckDriversEntryIDNumber);
        }

        public void setTruckDriversEntryIDNumber(OctetString newColValue) {
            //--AgentGen BEGIN=truckDriversEntry::setTruckDriversEntryIDNumber
            //--AgentGen END
            super.setValue(idxTruckDriversEntryIDNumber, newColValue);
        }

        @Override
        public Variable getValue(int column) {
            //--AgentGen BEGIN=truckDriversEntry::RowGetValue
            //--AgentGen END
            switch (column) {
                case idxTruckDriversEntryIndex:
                    return getTruckDriversEntryIndex();
                case idxTruckDriversEntryName:
                    return getTruckDriversEntryName();
                case idxTruckDriversEntryIDNumber:
                    return getTruckDriversEntryIDNumber();
                default:
                    return super.getValue(column);
            }
        }

        @Override
        public void setValue(int column, Variable value) {
            //--AgentGen BEGIN=truckDriversEntry::RowSetValue
            //--AgentGen END
            switch (column) {
                case idxTruckDriversEntryIndex:
                    setTruckDriversEntryIndex((Integer32) value);
                    break;
                case idxTruckDriversEntryName:
                    setTruckDriversEntryName((OctetString) value);
                    break;
                case idxTruckDriversEntryIDNumber:
                    setTruckDriversEntryIDNumber((OctetString) value);
                    break;
                default:
                    super.setValue(column, value);
            }
        }

        //--AgentGen BEGIN=truckDriversEntry::Row
        //--AgentGen END
    }

    class TruckDriversEntryRowFactory
            implements MOTableRowFactory<TruckDriversEntryRow> {

        @Override
        public synchronized TruckDriversEntryRow createRow(OID index, Variable[] values)
                throws UnsupportedOperationException {
            TruckDriversEntryRow row
                    = new TruckDriversEntryRow(index, values);
            //--AgentGen BEGIN=truckDriversEntry::createRow
            //--AgentGen END
            return row;
        }

        @Override
        public synchronized void freeRow(TruckDriversEntryRow row) {
            //--AgentGen BEGIN=truckDriversEntry::freeRow
            //--AgentGen END
        }

        //--AgentGen BEGIN=truckDriversEntry::RowFactory
        //--AgentGen END
    }

//--AgentGen BEGIN=_METHODS
//--AgentGen END
    // Textual Definitions of MIB module ApextruckingSnmpMib
    protected void addTCsToFactory(MOFactory moFactory) {
    }

//--AgentGen BEGIN=_TC_CLASSES_IMPORTED_MODULES_BEGIN
//--AgentGen END
    // Textual Definitions of other MIB modules
    public void addImportedTCsToFactory(MOFactory moFactory) {
    }

//--AgentGen BEGIN=_TC_CLASSES_IMPORTED_MODULES_END
//--AgentGen END
//--AgentGen BEGIN=_CLASSES
//--AgentGen END
//--AgentGen BEGIN=_END
//--AgentGen END
}
