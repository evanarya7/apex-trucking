package com.apextrucking.model;

/**
 *
 * @author defalt
 */
public class TruckDriver {

    private String name;
    private String idNumber;

    public TruckDriver() {
    }

    public TruckDriver(String name, String idNumber) {
        this.name = name;
        this.idNumber = idNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    @Override
    public String toString() {
        return "TruckDriver{" + "name=" + name + ", idNumber=" + idNumber + '}';
    }

}
